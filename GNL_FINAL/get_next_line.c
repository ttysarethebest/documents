/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: awicks <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/13 15:47:10 by awicks            #+#    #+#             */
/*   Updated: 2017/08/07 13:22:07 by awicks           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

static int		find_line(char *str)
{
	int i;
	int hold;

	hold = 0;
	i = 0;
	if (str == NULL)
		return (0);
	while (str[i] != '\0')
	{
		if (str[i] == '\n')
			hold = 1;
		i++;
	}
	if (hold == 1)
		return (1);
	else
		return (0);
}

static int		redefine(char *buff, int fd)
{
	char			temp[BUFF_SIZE + 1];
	static t_line	y;

	y.d = 0;
	y.c = 0;
	y.cfd2 = y.cfd1;
	y.cfd1 = fd;
	if (fd != y.cfd2)
	{
		ft_bzero(buff, BUFF_SIZE);
		return (y.cfd2);
	}
	ft_strcpy(temp, buff);
	while (temp[y.c] != '\n' && temp[y.c] != '\0')
		y.c++;
	if (temp[y.c])
		y.c++;
	while (temp[y.c])
	{
		buff[y.d] = temp[y.c];
		y.c++;
		y.d++;
	}
	buff[y.d] = '\0';
	return (y.cfd2);
}

static char		**append(char **dest, char *src, char con)
{
	char	*temp_dst;
	int		i;
	int		x;

	i = 0;
	x = 0;
	if (src == NULL)
		return (NULL);
	while (src[i] != con && src[i] != '\0')
		i++;
	if (*dest == NULL)
		ft_charcpy(dest, '\0');
	temp_dst = (char *)malloc(sizeof(char) * (ft_strlen(*dest) + i) + 1);
	ft_strcpy(temp_dst, *dest);
	ft_strlcat(temp_dst, src, i + ft_strlen(*dest) + 1);
	free(*dest);
	*dest = (char *)malloc(sizeof(char) * (ft_strlen(temp_dst)) + 1);
	ft_strcpy(*dest, temp_dst);
	free(temp_dst);
	return (NULL);
}

int				get_next_line(int fd, char **line)
{
	static t_line x;

	ft_ifstrdel(line, 1);
	if (fd != redefine(x.buff, fd))
		x.d = 0;
	if (*(x.buff))
		ft_strcpy(x.temp = malloc(ft_strlen(x.buff) + 1), x.buff);
	x.c = 1;
	while (find_line(x.temp) == 0 && x.c > 0)
	{
		if ((x.c = read(fd, x.buff, BUFF_SIZE)) < 0)
			return (x.c);
		x.buff[x.c] = '\0';
		if (x.c > 0)
			append(&(x.temp), x.buff, '\0');
		if (x.c == 0 && !x.temp)
			x.d = 2;
	}
	append(line, x.temp, '\n');
	if (*line == NULL)
		ft_charcpy(line, '\0');
	ft_ifstrdel(&(x.temp), 0);
	if (x.d == 2)
		return (0);
	return (1);
}
