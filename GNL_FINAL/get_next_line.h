/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: awicks <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/09 13:08:02 by awicks            #+#    #+#             */
/*   Updated: 2017/11/15 06:59:37 by awicks           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H

# define BUFF_SIZE 2056

# include "libft/libft.h"
# include <fcntl.h>
# include <unistd.h>

typedef struct	s_line
{
	char		buff[BUFF_SIZE + 1];
	char		*temp;
	int			d;
	int			c;
	int			cfd1;
	int			cfd2;
}				t_line;

int				get_next_line(int fd, char **line);

#endif
