/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_charcpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: awicks <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/14 14:58:47 by awicks            #+#    #+#             */
/*   Updated: 2017/08/03 11:31:43 by awicks           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdio.h>

char	*ft_charcpy(char **str, char a)
{
	char *temp;
	char *really;

	really = (char *)malloc(2);
	really[0] = a;
	really[1] = '\0';
	if (*str == NULL)
	{
		*str = (char *)malloc(2);
		ft_strcpy(*str, really);
		free(really);
	}
	else
	{
		temp = (char *)malloc(sizeof(char) * (ft_strlen(*str) + 2));
		ft_strcpy(temp, *str);
		ft_strcat(temp, really);
		free(*str);
		*str = (char *)malloc(sizeof(char) * (ft_strlen(temp) + 1));
		ft_strcpy(*str, temp);
		free(temp);
		free(really);
	}
	return (*str);
}
