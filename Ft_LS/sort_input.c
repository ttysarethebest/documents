/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   input test.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: awicks <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/02 11:24:24 by awicks            #+#    #+#             */
/*   Updated: 2017/07/08 10:18:07 by awicks           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

//static void		free_input(t_node *headi)
//{


static int		insert_flags(t_node *head, char **arg)
{
	int i;
	int j;

	i = 1;
	while (/*arg[i][0] == '-' && */arg[i] != '\0')
	{
		j = 1;
		if (arg[i][0] == '-')
			while (arg[i][j] != '\0')
			{
				if (arg[i][j] == 'a')
					head->a = 1;
				if (arg[i][j] == 'r')
					head->r = 1;
				if (arg[i][j] == 't')
					head->t = 1;
				if (arg[i][j] == 'l')
					head->l = 1;
				if (arg[i][j] == 'R')
					head->R = 1;
				j++;
			}
		else
			return (1);
		i++;
	}
	return (0);
}

static int		insert_command(t_node *head, char **arg)
{
	int i;
	int j;

	i = 1;
	j = 0;
	while (arg[j] != '\0')
		if (arg[j++][0] == '-')
			i++;
	if (!arg[i])
	{	
		ft_lst_newafter(head);
		ft_lst_search(head, 1)->command = (char *)ft_strnew(3);
		ft_strcpy(ft_lst_search(head, 1)->command, "./");
	}
	else
		while (arg[i] != '\0')
		{
			ft_lst_newafter(head);
			ft_lst_search(head, j)->command = (char *)ft_strnew((size_t)ft_strlen(arg[i]));
			ft_strcpy(ft_lst_search(head, j++)->command, arg[i++]);
		}
	return (j);
}

int			*sort_input(t_node *head, char **arg)
{
	insert_flags(head, arg);
	insert_command(head, arg);
	return (0);
}





//you need a list length
