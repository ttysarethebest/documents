/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: awicks <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/03 16:52:04 by awicks            #+#    #+#             */
/*   Updated: 2017/07/08 11:30:45 by awicks           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

#include <stdio.h>

int		sort_input(t_node *headi, char **arg);
int		read_recurse(char *pass, t_node *headi, t_node *heads);

int		main(int argc, char **argv)
{
	t_node *head;
	t_node *list;
	int i;
	
	i = 1;
	head = (t_node *)malloc(sizeof(t_node));
	head->next = NULL;
	list = (t_node *)malloc(sizeof(t_node));
	list->next = NULL;
	list ->child = NULL;
	sort_input(head, argv);
	//while (ft_lst_search(head, i)->next != NULL);	
	read_recurse(ft_lst_search(head, 1)->command, head, list);
	return (0);
}


