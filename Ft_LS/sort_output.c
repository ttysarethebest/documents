/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort_output.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: awicks <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/07 09:39:44 by awicks            #+#    #+#             */
/*   Updated: 2017/07/08 09:27:26 by awicks           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <time.h>

int		sort_time(long time, long *epoch, char *t_buff)
{
	char **time_t;
	int i;
	char *temp;
	
	i = 0;
	time_t = ft_strsplit(ctime(time));
	t_buff = (char *)malloc(17);
	ft_strcpy_buffer(temp, time_t[1], 4);
	ft_strcpy(t_buff, temp);
	ft_strclr(temp);
	ft_strcpy_buffer(temp, time_t[2], 3);
	ft_strcat(t_buff, temp);
	ft_strclr(temp);
	ft_strcpr_buffer(temp, time_t[3], 9);
	ft_strcat(t_buff, temp);
	free (temp);
	while (time_t[i] != NULL)
	{
		free (time_t[i]);
		i++;
	}
	free (time_t);
	time_t = NULL;
	*epoch = time;
	return (0);
}


