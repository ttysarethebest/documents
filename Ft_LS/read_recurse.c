/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: awicks <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/27 13:09:10 by awicks            #+#    #+#             */
/*   Updated: 2017/07/09 16:20:20 by awicks           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <dirent.h>
#include <sys/stat.h>
#include <time.h>
#include "libft.h"

#include <stdio.h>

int		check_valid(char *pass)
{
	int i;

	i = 0;
	while (pass[i] != '\0')
		i++;
	i--;
	if (pass[i] == '.' && (pass[i - 1] == '/' || pass[i - 1] == '.'))
		return (1);
	else
		return (0);
}

int		breakdown(char *pass, t_node *head, t_node *list)
{
	printf("%s\n", pass);
	//ft_putstr(pass);
	return (0);
}

char	*concat(char *child, char *pass, char *d_name)
{
	child = malloc(sizeof(d_name) + sizeof(pass) + 2);
	ft_strcpy(child, pass);
	ft_strcat(child, "/");
	ft_strcat(child, d_name);
	return (child);
}

int		read_recurse(char *pass, t_node *head, t_node *list)
{
	struct dirent *pDirent;
	DIR *pDir;
	char *child;

	breakdown(pass, head, list);
	pDir = opendir(pass);
	if (pDir == NULL || check_valid(pass) == 1)
		return (1);
	else
		while ((pDirent = readdir(pDir)) != NULL)
		{
			child = concat(child, pass, pDirent->d_name);
			if (ft_lst_search(head, 0)->R == 1)
				read_recurse(child, head, list);
			else
				breakdown(pDirent->d_name, head, list);
			free(child);
		}
	return (0);
}

/*int		main(int argc, char **argv)
{
	struct dirent *pDirent;
	DIR *pDir;
	struct stat file_info;

	pDir = opendir(argv[1]);
	if (pDir == NULL)
	{
		printf("CANNOT OPEN DIRECTORY");
		return (1);
	}

	while ((pDirent = readdir(pDir)) != NULL)
	{
		stat(pDirent->d_name, &file_info);
		//ft_putstr(ctime(&file_info.st_mtime));
		printf("%ld\n", file_info.st_mtime);
		//sort_time(ctime(&file_info.st_mtime));
	}
	closedir(pDir);
	return (0);
}*/
