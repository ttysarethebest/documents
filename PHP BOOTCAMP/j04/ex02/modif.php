<?php
if ($_POST["submit"] != "OK")
	return("ERROR\n");
if (file_get_contents("./private/passwd") == FALSE)
	return("ERROR\n");
if (file_get_contents("./private/passwd") == "NULL"){
    return ("ERROR\n");}
$_total = unserialize(file_get_contents("./private/passwd", "./private/passwd"));
if (!$_total[$_POST["login"]]){
	return("ERROR\n");}
if ($_total[$_POST["login"]]["passwd"] != hash("whirlpool", $_POST["oldpw"]))
	return ("ERROR\n");
if ($_POST["newpw"] =="")
	return("ERROR\n");
$_total[$_POST["login"]]["passwd"] = hash("whirlpool", $_POST["newpw"]);
file_put_contents("./private/passwd", serialize($_total));
return("OK\n");
?>