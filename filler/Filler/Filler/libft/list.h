/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: awicks <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/13 13:00:56 by awicks            #+#    #+#             */
/*   Updated: 2017/07/28 12:41:23 by awicks           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIST_H
# define LIST_H

typedef struct	s_stack
{
	int				a;
	int				b;
	int				atotal;
	int				btotal;
	struct s_node	*conta;
	struct s_node	*contb;
}				t_stack;

typedef struct	s_node
{
	int				value;
	int				pos;
	int				perc;
	int				despos;
	struct s_node	*next;
}				t_node;

#endif
