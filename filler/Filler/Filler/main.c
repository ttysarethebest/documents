/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: awicks <awicks@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/04 07:11:10 by awicks            #+#    #+#             */
/*   Updated: 2018/01/05 09:01:00 by awicks           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Includes/filler.h"

static t_pow	*get_player(void)
{
	char	*line;
	t_pow	*player;

	player = (t_pow *)malloc(sizeof(t_pow));
	get_next_line(0, &line);
	if (line[10] == '2')
	{
		player->small = 'x';
		player->big = 'X';
		player->nsmall = 'o';
		player->nbig = 'O';
	}
	else
	{
		player->small = 'o';
		player->big = 'O';
		player->nsmall = 'x';
		player->nbig = 'X';
	}
	return (player);
}

int				main(void)
{
	int		hold;
	t_play	*data;
	t_pow	*player;
	char	*co_ord;

	hold = 0;
	player = get_player();
	while (hold == 0)
	{
		data = create_field();
		data->small = player->small;
		data->big = player->big;
		data->nsmall = player->nsmall;
		data->nbig = player->nbig;
		co_ord = give_co_ord(data);
		ft_putstr(co_ord);
		if (ft_strstr(co_ord, "-1 -1\n") != NULL)
			hold = 1;
	}
	return (0);
}
