/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Filler.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: awicks <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/19 12:17:42 by awicks            #+#    #+#             */
/*   Updated: 2017/12/19 12:17:44 by awicks           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLER_H
# define FILLER_H

# include "../libft/libft.h"
# include "../GNL_FINAL/get_next_line.h"

typedef struct	s_play
{
	char		**map;
	char		**piece;
	char		small;
	char		big;
	char		nsmall;
	char		nbig;
}				t_play;

typedef struct	s_pow
{
	char		small;
	char		big;
	char		nsmall;
	char		nbig;
}				t_pow;

typedef struct	s_enem
{
	int			x;
	int			y;
	int			j;
	int			m;
	int			flag;
	int			*field;
	int			*co;
	int			diffx;
	int			diffy;

}				t_enem;

char			*is_valid(int x, int y, t_play *data);
t_play			*create_field(void);
char			*give_co_ord(t_play *data);
int				*field_size(t_play *data);
char			*ret_str(int x, int y);
int				abs(int x);
char			**rev_map(t_play *data);
char			*touch(int x, int y, t_play *data);
t_enem			ini_enem(t_play *data);
int				*basic(t_play *data, int j, int m);
int				*block_bottom(t_play *data, int j, int m);
int				*go_mid(t_play *data, int j, int m);
int				ar(int val, t_play *data, int k);

#endif
