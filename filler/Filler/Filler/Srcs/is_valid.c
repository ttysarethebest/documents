/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   is_valid.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: awicks <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/02 11:32:25 by awicks            #+#    #+#             */
/*   Updated: 2018/01/02 11:32:27 by awicks           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../Includes/filler.h"

int		findx(t_play *data)
{
	int	x;
	int y;
	int max;

	y = 0;
	max = 0;
	while (data->piece[y] != NULL)
	{
		if (ft_strstr(data->piece[y], "*") == NULL)
			y++;
		else
		{
			x = 0;
			while (data->piece[y][x] && data->piece[y][x] != '*')
				x++;
			while (data->piece[y][x] && data->piece[y][x] == '*')
				x++;
			y++;
		}
		if (x > max)
			max = x;
	}
	return (max);
}

int		findy(t_play *data)
{
	int	y;
	int	dud;

	dud = 0;
	y = 0;
	while (data->piece[y] != NULL && ft_strstr(data->piece[y], "*") == NULL)
		y++;
	while (data->piece[y] != NULL)
	{
		if (ft_strstr(data->piece[y], "*") == NULL)
			dud++;
		y++;
	}
	return (y - dud);
}

char	*touch_border(int x, int y, t_play *data)
{
	int xpiece;
	int ypiece;
	int	xmap;
	int ymap;

	xmap = 0;
	ymap = 0;
	xpiece = findx(data);
	ypiece = findy(data);
	while (data->map[ymap][xmap])
		xmap++;
	while (data->map[ymap] != NULL)
		ymap++;
	if (x + xpiece > xmap)
		return (NULL);
	if (y + ypiece > ymap)
		return (NULL);
	return ("TRUE");
}

char	*touch_piece(int x, int y, t_play *data)
{
	int flag;
	int	i;
	int j;

	flag = 0;
	j = 0;
	while (data->piece[j] != NULL)
	{
		i = 0;
		while (data->piece[j][i])
		{
			if (data->piece[j][i] == '*' && (data->map[y + j][x + i] ==
				data->nsmall || data->map[y + j][x + i] == data->nbig))
				return (NULL);
			if (data->piece[j][i] == '*' && (data->map[y + j][x + i] ==
				data->small || data->map[y + j][x + i] == data->big))
				if (++flag > 1)
					return (NULL);
			i++;
		}
		j++;
	}
	if (flag == 0 || flag > 1)
		return (NULL);
	return ("TRUE");
}

char	*is_valid(int x, int y, t_play *data)
{
	if (touch_border(x, y, data) == NULL)
		return (NULL);
	if (touch_piece(x, y, data) == NULL)
		return (NULL);
	return ("TRUE");
}
