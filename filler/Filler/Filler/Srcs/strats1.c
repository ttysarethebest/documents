/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strats1.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: awicks <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/10 09:49:27 by awicks            #+#    #+#             */
/*   Updated: 2018/01/10 09:49:29 by awicks           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../Includes/filler.h"

int		*ini_dco(int x, int y, int flag)
{
	int	*dco;

	dco = (int *)malloc(sizeof(int) * 3);
	dco[0] = y;
	dco[1] = x;
	dco[2] = flag;
	return (dco);
}

int		*basic(t_play *data, int j, int m)
{
	t_enem d;

	d = ini_enem(data);
	while (d.y <= d.field[0] && d.flag == 0)
	{
		d.x = 0;
		while (d.x <= d.field[1] && d.flag == 0)
		{
			if (is_valid(abs(d.x - j), abs(d.y - m), data) != NULL)
				d.flag = 1;
			if (d.flag == 0)
				d.x++;
		}
		if (d.flag == 0)
			d.y++;
	}
	d.co = ini_dco(d.x, d.y, d.flag);
	return (d.co);
}

int		*block_bottom(t_play *data, int j, int m)
{
	t_enem d;

	d = ini_enem(data);
	d.y = d.field[0];
	while (d.y >= 0 && d.flag == 0 && touch(abs(d.field[1] - j - 1),
		abs(d.field[0] - m), data) == NULL)
	{
		d.x = d.field[1];
		while (d.x >= 0 && d.flag == 0)
		{
			if (is_valid(abs(d.x - j), abs(d.y - m), data) != NULL)
				d.flag = 1;
			if (d.flag == 0)
				d.x--;
		}
		if (d.flag == 0)
			d.y--;
	}
	d.co = ini_dco(d.x, d.y, d.flag);
	return (d.co);
}

int		*go_mid(t_play *data, int j, int m)
{
	t_enem d;

	d = ini_enem(data);
	d.y = (d.field[0] * 2 / 3) + 1;
	while (d.y != (d.field[0] * 2 / 3) && d.flag == 0 && touch(abs(0 - j),
		abs((d.field[0] * 2 / 3) - m), data) == NULL)
	{
		if (d.y == d.field[0] + 1)
			d.y = 0;
		d.x = 0;
		while (d.x <= d.field[1] && d.flag == 0)
		{
			if (is_valid(abs(d.x - j), abs(d.y - m), data) != NULL)
				d.flag = 1;
			if (d.flag == 0)
				d.x++;
		}
		if (d.flag == 0)
			d.y++;
	}
	d.co = ini_dco(d.x, d.y, d.flag);
	return (d.co);
}
