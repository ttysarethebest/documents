/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   co_ord_util.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: awicks <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/04 10:42:34 by awicks            #+#    #+#             */
/*   Updated: 2018/01/04 10:42:37 by awicks           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../Includes/filler.h"

char	*ret_str(int x, int y)
{
	char	*temp;
	int		i;
	char	*result;

	result = NULL;
	temp = ft_itoa(y);
	i = 0;
	while (temp[i])
		ft_charcpy(&result, temp[i++]);
	ft_charcpy(&result, ' ');
	i = 0;
	temp = ft_itoa(x);
	while (temp[i])
		ft_charcpy(&result, temp[i++]);
	ft_charcpy(&result, '\n');
	return (result);
}

int		*field_size(t_play *data)
{
	int y;
	int x;
	int *co_map;

	y = 0;
	x = 0;
	co_map = (int *)malloc(sizeof(int) * 2);
	while (data->map[y][x])
		x++;
	while (data->map[y] != NULL)
		y++;
	co_map[0] = y - 1;
	co_map[1] = x - 1;
	return (co_map);
}

int		abs(int x)
{
	if (x < 0)
		x = x * -1;
	return (x);
}

char	*touch(int x, int y, t_play *data)
{
	if (data->map[y][x] == 'x' || data->map[y][x] == 'X')
		return ("FRIEND");
	if (data->map[y][x] == 'o' || data->map[y][x] == 'O')
		return ("ENEMY");
	if (x == 0 || x == field_size(data)[(y = -1) * -1])
		while (++y <= field_size(data)[0])
		{
			if (data->map[y][x] == 'x' || data->map[y][x] == 'X')
				return ("FRIEND");
			if (data->map[y][x] == 'o' || data->map[y][x] == 'O')
				return ("ENEMY");
		}
	if (y == 0 || y == field_size(data)[0])
	{
		x = -1;
		while (++x <= field_size(data)[1])
		{
			if (data->map[y][x] == 'x' || data->map[y][x] == 'X')
				return ("FRIEND");
			if (data->map[y][x] == 'o' || data->map[y][x] == 'O')
				return ("ENEMY");
		}
	}
	return (NULL);
}

t_enem	ini_enem(t_play *data)
{
	t_enem d;

	d.x = 0;
	d.y = 0;
	d.flag = 0;
	d.j = 0;
	d.m = 0;
	d.diffx = 0;
	d.diffy = 0;
	d.field = field_size(data);
	if ((data->small != 'x' && d.field[0] <= 23) ||
		(data->small == 'x' && d.field[0] >= 24))
	{
		d.j = d.field[1];
		d.m = d.field[0];
	}
	return (d);
}
