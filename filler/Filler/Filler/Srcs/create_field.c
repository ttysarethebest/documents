/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   create_field.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: awicks <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/19 15:48:03 by awicks            #+#    #+#             */
/*   Updated: 2017/12/19 15:48:06 by awicks           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../Includes/filler.h"

static char		**pull_piece(char **raw)
{
	int		i;
	int		x;
	int		j;
	char	**piece;

	i = 0;
	x = 0;
	j = 0;
	while (ft_strstr(raw[i], "Piece") == NULL)
		i++;
	i++;
	while (raw[i + x])
		x++;
	piece = (char **)malloc(sizeof(char *) * x + 1);
	while (raw[i])
	{
		x = 0;
		piece[j] = NULL;
		while (raw[i][x])
			ft_charcpy(&(piece[j]), raw[i][x++]);
		i++;
		j++;
	}
	piece[j] = NULL;
	return (piece);
}

static char		**pull_map(char **raw)
{
	char	**map;
	int		i;
	int		x;
	int		j;

	i = 1;
	j = 0;
	while (ft_strstr(raw[i], "Piece") == NULL)
		i++;
	map = (char **)malloc(sizeof(char *) * (i - 1) + 1);
	i = 1;
	while (raw[i] && ft_strstr(raw[i], "Piece") == NULL)
	{
		x = 4;
		map[j] = NULL;
		while (raw[i][x])
			ft_charcpy(&(map[j]), raw[i][x++]);
		j++;
		i++;
	}
	map[j] = NULL;
	return (map);
}

static int		check_piece(char *str)
{
	int		i;
	int		x;
	char	temp[5];

	i = 6;
	x = 0;
	while (str[i] != ' ')
	{
		temp[x] = str[i];
		i++;
		x++;
	}
	temp[x] = '\0';
	return (ft_atoi(temp));
}

static char		**find_raw_field(void)
{
	char	*line;
	char	**raw;
	char	*temp;
	int		m;
	int		x;

	m = 0;
	x = 10000000;
	temp = NULL;
	while (get_next_line(0, &line) > 0)
		if (ft_strstr(line, "Plateau") != NULL)
			break ;
	while (x > 0)
	{
		m = 0;
		get_next_line(0, &line);
		if (ft_strstr(line, "Piece") != NULL)
			x = check_piece(line) + 1;
		while (line[m])
			ft_charcpy(&temp, line[m++]);
		ft_charcpy(&temp, '\n');
		x--;
	}
	raw = ft_strsplit(temp, '\n');
	return (raw);
}

t_play			*create_field(void)
{
	char	**raw;
	t_play	*mapping;

	if ((raw = find_raw_field()) == NULL)
		return (NULL);
	mapping = (t_play *)malloc(sizeof(t_play));
	mapping->map = pull_map(raw);
	mapping->piece = pull_piece(raw);
	return (mapping);
}
