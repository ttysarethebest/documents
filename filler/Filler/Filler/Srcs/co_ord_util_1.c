/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   co_ord_util_1.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: awicks <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/10 15:14:01 by awicks            #+#    #+#             */
/*   Updated: 2018/01/10 15:14:04 by awicks           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../Includes/filler.h"

int		ar(int val, t_play *data, int k)
{
	float	x;
	int		j;

	if (k == 1)
		x = ((float)val / (float)39) * (float)(field_size(data)[k]);
	else
		x = ((float)val / (float)23) * (float)(field_size(data)[k]);
	j = (int)(x < 0 ? (x - 0.5) : (x + 0.5));
	return (j);
}
