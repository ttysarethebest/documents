/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   give_co_ord.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: awicks <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/02 11:32:06 by awicks            #+#    #+#             */
/*   Updated: 2018/01/02 11:32:11 by awicks           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../Includes/Filler.h"

char	*give_co_ord(t_play *data)
{
	t_enem d;

	d = ini_enem(data);
	if (d.field[0] >= 23)
	{
		d.co = go_mid(data, d.j, d.m);
		if (d.co[2] == 0)
			d.co = block_bottom(data, d.j, d.m);
	}
	else
		d.co = block_bottom(data, d.j, d.m);
	if (d.co[2] == 0)
		d.co = basic(data, d.j, d.m);
	if (data->map[abs(d.co[0] - d.m)] == NULL || d.co[2] == 0)
		return ("-1 -1\n");
	return (ret_str(abs(d.co[1] - d.j), abs(d.co[0] - d.m)));
}
