/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strats2.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: awicks <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/10 16:24:53 by awicks            #+#    #+#             */
/*   Updated: 2018/01/10 16:24:56 by awicks           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../Includes/filler.h"

int		*reach_left_down(t_play *data, int j, int m)
{
	t_enem d;

	d = ini_enem(data);
	d.y = ar(20, data, 0);
	while (data->map[d.y] != NULL && d.flag == 0 &&
		touch(0, ar(19, data, 0), data) == NULL && d.y != ar(19, data, 0))
	{
		if (d.y == ar(23, data, 0))
			d.y = 0;
		while (data->map[d.y][d.x] && d.flag == 0)
		{
			d.diffy = -4;
			while (d.diffy != 5 && d.flag == 0)
			{
				if (is_valid(abs(d.x - j), abs(d.y - m), data) != NULL)
				{	
					d.flag = 1;
					d.diffy = 0;
				}
				else if (is_valid(abs(d.x - j), abs((d.y + d.diffy) - m), data) != NULL)
					d.flag = 1;
				if (d.flag == 0)
					d.diffy++;
			}
			if (d.flag == 0)
				d.x++;
		}
		if (d.flag == 0)
			d.y++;
	}
	d.co = ini_dco(d.x, (d.y + d.diffy), d.flag)
	return (d.co);
}

int		*reach_up_right(t_play *data, int j, int m)
{
	t_enem d;

	d = ini_enem(data);
	d.x = ar(29, data, 1);
	while (d.flag == 0 && d.x != ar(28, data, 1) && touch(ar(29, data, 1), 0, data) == NULL)
	{
		if (d.x == ar(39, data, 1))
			d.x = 0;
		d.y = 0;
		while (data->map[d.y] != NULL && d.flag == 0)
		{
			d.diffx = -4;
			while (diffx != 5 && d.flag == 0)
			{
				if (is_valid(abs(d.x - j), abs(d.y - m), data) != NULL)
				{	
					d.flag = 1;
					d.diffx = 0;
				}
				else if (is_valid(abs((d.x + d.diffx) - j), abs(d.y - m), data) != NULL)
					d.flag = 1;
				if (d.flag == 0)
					d.diffx++;
			}
			if (d.flag == 0)
				d.y++;
		}
		if (d.flag == 0)
				d.x++;
	}
	d.co = ini_dco(d.x + d.diffx, d.y, d.flag);
	return (d.co);
}

int		*pattern_a(t_play *data, int j, int m)
{
	t_enem d;

	d = ini_enem(data);
	d.y = ar(12, data, 0);
	while (d.y != ar(11, ))
}


/*diff = 0;
diffx = 0;

	if (d.flag == 0)
	{
		d.y = 20;
		while (data->map[d.y] != NULL && d.flag == 0 && touch(0, 19, data) == NULL && d.y != 19)
		{
			if (d.y == 23)
				d.y = 
			d.x = 0;
			while (data->map[d.y][d.x] && d.flag == 0)
			{
				diff = -4;
				while (diff != 5 && d.flag == 0)
				{	
					if (is_valid(abs(d.x - d.j), abs((d.y) - d.m), data) != NULL)
					{	
						d.flag = 1;
						diff = 0;
					}
					else if (is_valid(abs(d.x - d.j), abs((d.y + diff) - d.m), data) != NULL)
						d.flag = 1;
					if (d.flag == 0)
						diff++;
				}
				if (d.flag == 0)
					d.x++;
			}
			if (d.flag == 0)
				d.y++;
		}
	}

	if (d.flag == 0)
	{
		d.x = 29;
		d.y = 0;
		while (d.flag == 0 && d.x != 28 && touch(29, 0, data) == NULL)
		{
			if (d.x == 39)
				d.x = 0;
			d.y = 0;
			while (data->map[d.y] != NULL && d.flag == 0)
			{
				diffx = -4;
				while (diffx != 5 && d.flag ==0)
				{
					if (is_valid(abs(d.x - d.j), abs(d.y - d.m), data) != NULL)
					{	
						d.flag = 1;
						diffx = 0;
					}
					else if (is_valid(abs((d.x + diffx) - d.j), abs(d.y - d.m), data) != NULL)
						d.flag = 1;
					if (d.flag == 0)
						diffx++;
				}
				if (d.flag == 0)
					d.y++;
			}
			if (d.flag == 0)
				d.x++;
		}
	}

	if (d.flag == 0)
	{
		d.y = 12;
		while (data->map[d.y] != NULL && d.flag == 0 && touch(12, 13, data) == NULL && d.y != 11)
		{
			if (d.y == 23)
				d.y = 0;
			d.x = 0;
			while (data->map[d.y][d.x] && d.flag == 0)
			{
				diff = -2;
				while (diff != 3 && d.flag == 0)
				{	
					if (is_valid(abs(d.x - d.j), abs((d.y) - d.m), data) != NULL)
					{	
						d.flag = 1;
						diff = 0;
					}
					else if (is_valid(abs(d.x - d.j), abs((d.y + diff) - d.m), data) != NULL)
						d.flag = 1;
					if (d.flag == 0)
						diff++;
				}
				if (d.flag == 0)
					d.x++;
			}
			if (d.flag == 0)
				d.y++;
		}
	}

	if (d.flag == 0)
	{
		diff = 0;
		diffx = 0;
		d.x = 19;
		d.y = 11;

		while ((d.x +  diffx) >= 0 && (d.y +  diff) >= 0 && data->map[d.y + diff] != NULL && data->map[d.y + diff][d.x + diffx] && d.flag == 0)
		{

			if (is_valid(d.x, d.y + diff, data) != NULL)
			{	
				d.flag = 1;
				diffx = 0;
			}
			else if (is_valid(d.x + diffx, d.y, data) != NULL)
			{	
				d.flag = 1;
				diff = 0;
			}
			else if (is_valid(d.x + diffx, d.y + diff, data) != NULL)
				d.flag = 1;
			else if (diffx == 0)
			{
				diffx++;
				diff++;
			}
			else if (diffx > 0 && diff > 0)
				diffx = diffx * -1;
			else if (diffx < 0 && diff > 0)
			{
				diffx = diffx * -1;
				diff = diff * -1;
			}
			else if (diff < 0 && diffx > 0)
				diffx = diffx * -1;
			else if (diff < 0 && diffx < 0)
			{
				diffx = diffx * -1;
				diff = diff * -1;
				diffx++;
				diff++;
			}

		}
	}

	if (d.flag == 0)
	{
		diff = 0;
		diffx = 0;
		d.y = 0;
		while (d.y <= 23 && d.flag == 0)
		{
			d.x = 0;
			while (d.x <= 39 && d.flag == 0)
			{
				if (is_valid(abs(d.x - d.j), abs(d.y - d.m), data) != NULL)
					d.flag = 1;
				if (d.flag == 0)
			if (d.flag == 0)
					d.x++;
			}
			if (d.flag == 0)
				d.y++;
		}
	}

if (data->map[abs((d.y + diff) - d.m)] == NULL || is_valid(abs((d.x + diffx) - d.j), abs((d.y + diff) - d.m), data) == NULL)
		return ("-1 -1\n");
	return (ret_str(abs((d.x + diffx) - d.j), abs((d.y + diff) - d.m)));*/

















