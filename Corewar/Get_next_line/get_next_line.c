/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: awicks <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/13 15:47:10 by awicks            #+#    #+#             */
/*   Updated: 2017/08/07 13:22:07 by awicks           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"
#include <stdio.h>

static int		read_buffer(char **remain, int fd)
{
	int		m;
	char	*temp;
	char	*temp2;

	m = BUFF_SIZE;
	temp = (char *)malloc(sizeof(char) * BUFF_SIZE + 1);
	while (m > 0)
	{
		m = read(fd, temp, BUFF_SIZE);
		temp[m] = '\0';
		if (*remain == NULL)
			ft_charcpy(remain, '\0');
		temp2 = (char *)malloc(sizeof(char) * (ft_strlen(*remain)) + 1);
		ft_strcpy(temp2, *remain);
		free(*remain);
		*remain = (char *)malloc(sizeof(char) * (ft_strlen(temp) +
			ft_strlen(temp2)) + 1);
		ft_strcpy(*remain, temp2);
		ft_strcat(*remain, temp);
		free(temp2);
	}
	free(temp);
	return (m);
}

int				get_next_line(int fd, char **line)
{
	static char *remain;
	static int	i;
	static int	hold;

	if (remain == NULL)
		hold = read_buffer(&remain, fd);
	if (*line != NULL)
		free(*line);
	*line = NULL;
	if (remain[i] == '\0' || (remain[i] == '\n' && remain[i + 1] == '\0'))
	{
		free(remain);
		return (hold);
	}
	if (remain[i] == '\n')
		i++;
	while (remain[i] != '\n' && remain[i] != '\0')
		ft_charcpy(line, remain[i++]);
	if (*line == NULL)
		ft_charcpy(line, '\0');
	return (1);
}
