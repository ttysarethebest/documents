/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   setup.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: awicks <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/04 15:08:35 by awicks            #+#    #+#             */
/*   Updated: 2017/09/04 15:08:37 by awicks           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SETUP_H
# define SETUP_H

#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include "../op.h"
#include "../libft/libft.h"
#include "../Get_next_line/get_next_line.h"

typedef struct  s_pop
{
	char *name;
	char *comment;	
}				t_pop;

#endif