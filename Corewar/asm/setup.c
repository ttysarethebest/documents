/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   setup.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: awicks <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/04 10:37:35 by awicks            #+#    #+#             */
/*   Updated: 2017/09/04 10:37:40 by awicks           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "setup.h"

int		create_file(char *name)
{
	char *filename;
	int fd_dest;
	mode_t mode;

	mode = S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;
	filename = (char *)malloc(sizeof(char) * ft_strlen(name) + 5);
	ft_strcpy(filename, name);
	ft_strcat(filename, ".cor");
	fd_dest = open(filename, O_RDWR | O_CREAT | O_TRUNC, mode);
	free(filename);
	filename = NULL;
	return (fd_dest);
}

int		populate(int fd_source, t_pop *setup_data)
{
	char *line;
	char *temp;
	int i;

	i = 0;
	line = NULL;
	get_next_line(fd_source, &line);
	if (ft_strstr(line, NAME_CMD_STRING) != NULL)
	{	
		while (line[i] != '\"')
			i++;
		while (line[i] != '\"')
			ft_charcpy(&(setup_data->name), line[i]);
	}
	if (line != NULL)
		free(line);
	get_next_line(fd_source, &line);
	i = 0;
	if (ft_strstr(line, COMMENT_CMD_STRING))
	while (line[i] != '\"')
		i++;
	while (line[i] != '\"')
		ft_charcpy(&(setup_data->name), line[i]);
	return (0);
}

int		main(int argc, char **argv)
{
	int fd_dest;
	int fd_source;
	t_pop *setup_data;

	setup_data = (t_pop *)malloc(sizeof(t_pop));
	fd_dest = create_file(argv[2]);
	fd_source = open(argv[2], O_RDWR);
	populate(fd_source, setup_data);
	return (0);
}
/*int		write_to_file(int fd_dest, t_pop *setup_data)
{

}*/