/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: awicks <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/16 13:40:06 by awicks            #+#    #+#             */
/*   Updated: 2017/07/28 15:07:23 by awicks           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strlcat(char *dest, const char *src, size_t size)
{
	size_t		x;
	int			i;
	size_t		j;
	size_t		len;
	int			y;

	i = 0;
	j = 0;
	x = size;
	y = ft_strlen(dest);
	while (x-- != 0 && dest[j] != '\0')
		j++;
	len = j;
	if ((x = size - len) == 0)
		return (j + (size_t)ft_strlen(src));
	while (src[i] != '\0' && x-- != 1)
		dest[j++] = src[i++];
	dest[j] = '\0';
	return (y + ft_strlen(src));
}
