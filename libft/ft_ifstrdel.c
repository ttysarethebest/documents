/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ifstrdel.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: awicks <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/01 13:01:16 by awicks            #+#    #+#             */
/*   Updated: 2017/10/01 13:01:21 by awicks           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdio.h>

char	**ft_ifstrdel(char **as, int m)
{
	if (as == ((void*)0))
		return (NULL);
	if (m == 1)
	{
		*as = NULL;
		return (as);
	}
	if (*as)
	{
		free(*as);
		*as = NULL;
		return (as);
	}
	else
		*as = NULL;
	return (as);
}
